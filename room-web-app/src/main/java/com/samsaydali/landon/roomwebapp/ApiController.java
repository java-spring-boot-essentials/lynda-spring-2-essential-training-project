package com.samsaydali.landon.roomwebapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/rooms")
public class ApiController {
    private RoomService roomService;

    @Autowired
    public ApiController(RoomService roomService) {
        super();
        this.roomService = roomService;
    }

    @GetMapping
    public ArrayList<Room> getAllRooms() {
        return this.roomService.getAllRooms();
    }
}
